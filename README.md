## Current Mods


* [Bears of the North](https://www.nexusmods.com/skyrimspecialedition/mods/47541)
    * Improved Bear models/skins
* [Immersive Interactions](https://www.nexusmods.com/skyrimspecialedition/mods/47670)
* Animations for interacting with the world.
* [Smooth Cam](https://www.nexusmods.com/skyrimspecialedition/mods/41252)
    * 3rd Person Camera
* [Smooth Cam: Modern Preset](https://www.nexusmods.com/skyrimspecialedition/mods/41636)
* [Underdog Animations](https://www.nexusmods.com/skyrimspecialedition/mods/51811)
    * Replacement for Movement Behavior Overhaul
* [EVG Conditional Idles](https://www.nexusmods.com/skyrimspecialedition/mods/34006)
    * Good Idle Animations
* [DAR - Dynamic Animation Replacer](https://www.nexusmods.com/skyrimspecialedition/mods/33746)
* [Better Combat Escape](https://www.nexusmods.com/skyrimspecialedition/mods/43936)
    * Lose Aggo better when escaping a combat
* [Audio Overhaul for Skyrim SE](https://www.nexusmods.com/skyrimspecialedition/mods/12466/)
* [Floating Damage](https://www.nexusmods.com/skyrimspecialedition/mods/14332)
* [Better Floating Damage Preset](https://www.nexusmods.com/skyrimspecialedition/mods/47523)
* [Oblivion Camera](https://www.nexusmods.com/skyrimspecialedition/mods/39412)

## Mods to Add

### Weapons

* [Jaysus Swords](https://www.nexusmods.com/skyrimspecialedition/mods/29415)
* [Royal Armory - New Artifacts](https://www.nexusmods.com/skyrimspecialedition/mods/6994)
* [Lore Weapon Expansion SE](https://www.nexusmods.com/skyrimspecialedition/mods/9660)
* [Reforging - To the Masses (Weapons Expansion)](https://www.nexusmods.com/skyrimspecialedition/mods/49030)

### Armors

* [Project NordwarUA](https://www.nexusmods.com/skyrimspecialedition/mods/43229)
    * Search for NordwarUA for other mods
* [Immersive Armors - Realistic Armors replace SE](https://www.nexusmods.com/skyrimspecialedition/mods/36746)
* [Common Clothes and Armors](https://www.nexusmods.com/skyrimspecialedition/mods/21305)

### Weapons and Armors

* [Ulag's Legacy](https://www.nexusmods.com/skyrimspecialedition/mods/20510)

## Mods to Look At

* [Cathedral Armory](https://www.nexusmods.com/skyrimspecialedition/mods/20199)
    * Improves armors
* [Lighten Skyrim](https://www.nexusmods.com/skyrimspecialedition/mods/50755)
    * Removes some Garabge objects for better FPS
* [Completionist - Quest Tracker](https://www.nexusmods.com/skyrimspecialedition/mods/46358)
* [Splashes of Skyrim](https://www.nexusmods.com/skyrimspecialedition/mods/47710)
    * Adds water effects, maybe worth the effort
* [RDO MCM](https://www.nexusmods.com/skyrimspecialedition/mods/44601)
* [EVG Animation Variance](https://www.nexusmods.com/skyrimspecialedition/mods/38534)
* [Nether's Follower Framework](https://www.patreon.com/posts/nethers-follower-53262094)
    * Alt for Amazing Follower Tweaks, looks like a good one.
* [Immervise World Encounters](https://www.nexusmods.com/skyrimspecialedition/mods/18330)
* [Wildcat](https://www.nexusmods.com/skyrimspecialedition/mods/1368)
* [Ultimate Combat](https://www.nexusmods.com/skyrimspecialedition/mods/17196)
* [Mortal Enemies](https://www.nexusmods.com/skyrimspecialedition/mods/4881)
* [Customizable Camera](https://www.nexusmods.com/skyrimspecialedition/mods/12201)
* [Forgotten Retex Project](https://www.nexusmods.com/skyrimspecialedition/mods/7849/)

* [Tamrielic Landscapes](https://www.nexusmods.com/skyrimspecialedition/mods/32973)
* [Septentrional Landscapes SE](https://www.nexusmods.com/skyrimspecialedition/mods/29842)
* [Hyperborean Snow SE](https://www.nexusmods.com/skyrimspecialedition/mods/29283)
* [Northern Shores SE](https://www.nexusmods.com/skyrimspecialedition/mods/27041)

## Modding guide

* [SKYRIM : SPECIAL EDITION 2021 GRAPHICS TUTORIAL](https://www.predcaliber.com/skyrimse2021-10years)
* [Modding 2021](https://www.nexusmods.com/skyrimspecialedition/mods/40407)
* [Goonz 2021 Mod List with Modding Tutorial](https://steamcommunity.com/sharedfiles/filedetails/?id=2189110883)

## Mods to Remember

* [Nordic UI](https://www.nexusmods.com/skyrimspecialedition/mods/49881)
    * UI Mod if Paper is dumb
* [Scrambled Eggs - Bugfixes](https://www.nexusmods.com/skyrimspecialedition/mods/43532)
* [SMIM Addons](https://www.nexusmods.com/skyrimspecialedition/mods/44388)
    * Makes objects more like metal, meh
* [Ruin Clutter Fix](https://www.nexusmods.com/skyrimspecialedition/mods/21031)
    * Really minor fixes, meh
* [Cathedral - 3D Pine Grass](https://www.nexusmods.com/skyrimspecialedition/mods/42032)
* [True Storms](https://www.nexusmods.com/skyrimspecialedition/mods/2472)
    * Is it compatible with Cathedral Weathers? Not as of July 31/21
* [Paper sounds for UI](https://www.nexusmods.com/skyrimspecialedition/mods/38944)
* [Inpa Sekiro Combat](https://www.nexusmods.com/skyrimspecialedition/mods/41428)
    * Souls like Combat, Not for me at this time.
* [Sounds of Skyrim](https://www.nexusmods.com/skyrimspecialedition/mods/8286)
